package com.example.minesweeper;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class StatusPanel extends JPanel implements Runnable {
    private JLabel timeElapsed;
    private JLabel markedMineCount;
    private int timeElapsedInSeconds;
    private int minesMarked;
    private ScheduledExecutorService executorService;
    private boolean isTimerStarted;

    public StatusPanel() {
        timeElapsed = new JLabel();
        markedMineCount = new JLabel();
        timeElapsed.setPreferredSize(new Dimension(100, 20));
        markedMineCount.setPreferredSize(new Dimension(100, 20));
        timeElapsed.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        markedMineCount.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        setLayout(new FlowLayout());
        add(new JLabel("Time Elapsed"));
        add(timeElapsed);
        add(new JLabel("Mines Selected"));
        add(markedMineCount);
        setTime();
        updateMineCount();
        setVisible(true);
    }

    public void startTimer() {
        if (!isTimerStarted) {
            executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(this, 1, 1, TimeUnit.SECONDS);
            isTimerStarted = true;
        }
    }

    @Override
    public void run() {
        timeElapsedInSeconds++;
        setTime();
    }

    public void stopTimer() {
        executorService.shutdownNow();
        isTimerStarted = false;
    }

    public void incrementMineSelection() {
        minesMarked++;
        updateMineCount();
    }

    public void decrementMineSelection() {
        minesMarked--;
        updateMineCount();
    }

    public void reset() {
        minesMarked = 0;
        isTimerStarted = false;
        timeElapsedInSeconds = 0;
        if (executorService != null && !executorService.isShutdown()) {
            executorService.shutdownNow();
            executorService = null;
        }
        updateMineCount();
        setTime();
    }

    private void updateMineCount() {
        invalidate();
        markedMineCount.setText(String.valueOf(minesMarked));
        revalidate();
    }

    private void setTime() {
        invalidate();
        final int hour = timeElapsedInSeconds / 3600;
        final int minute = (timeElapsedInSeconds % 3600) / 60;
        LocalTime time = LocalTime.of(hour, minute, timeElapsedInSeconds % 60);
        timeElapsed.setText(time.format(DateTimeFormatter.ISO_TIME));
        revalidate();
    }
}
