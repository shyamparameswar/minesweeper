package com.example.minesweeper;

public enum DifficultyLevel {
    LOW(10, 10, 25), MEDIUM(10, 12, 50), HIGH(12, 15, 75);

    private final int rowCount;
    private final int columnCount;
    private final int numberOfMines;

    private DifficultyLevel(int rowCount, int columnCount, int numberOfMines) {
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        this.numberOfMines = numberOfMines;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getNumberOfMines() {
        return numberOfMines;
    }
}
