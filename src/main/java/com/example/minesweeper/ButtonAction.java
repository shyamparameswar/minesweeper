package com.example.minesweeper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

public class ButtonAction implements Action {
    private Map<String, Integer> coordinates = new HashMap<>();

    @Override
    public Object getValue(String key) {
        return coordinates.get(key);
    }

    @Override
    public void putValue(String key, Object value) {
        if (value instanceof Integer) {
            coordinates.put(key, (Integer) value);
        }
    }

    @Override
    public void setEnabled(boolean b) {

    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {

    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {

    }

    @Override
    public void actionPerformed(ActionEvent event) {

    }
}
