package com.example.minesweeper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MineSweeperMenuBar extends JMenuBar implements ActionListener {
    public static final String CMD_RESET_GAME_EVENT = "ResetGame";
    public static final String CMD_SELECT_LOW_DIFF = "DifficultyLow";
    public static final String CMD_SELECT_MED_DIFF = "DifficultyMed";
    public static final String CMD_SELECT_HIGH_DIFF = "DifficultyHigh";
    private MenuCallBackHandler callBackHandler;
    private JMenu fileMenu;
    private JMenu difficultyLevelMenu;
    private JMenuItem resetGameMenuItem;
    private JMenuItem lowDifficultyMenuItem;
    private JMenuItem mediumDifficultyMenuItem;
    private JMenuItem highDifficultyMenuItem;

    public MineSweeperMenuBar(MenuCallBackHandler callBackHandler) {
        this.callBackHandler = callBackHandler;
        fileMenu = new JMenu("File");
        add(fileMenu);
        resetGameMenuItem = new JMenuItem("Reset");
        resetGameMenuItem.addActionListener(this);
        resetGameMenuItem.setActionCommand(CMD_RESET_GAME_EVENT);
        fileMenu.add(resetGameMenuItem);
        difficultyLevelMenu = new JMenu("Difficulty");
        fileMenu.add(difficultyLevelMenu);
        lowDifficultyMenuItem = new JMenuItem("Low");
        lowDifficultyMenuItem.setActionCommand(CMD_SELECT_LOW_DIFF);
        lowDifficultyMenuItem.addActionListener(this);
        difficultyLevelMenu.add(lowDifficultyMenuItem);
        mediumDifficultyMenuItem = new JMenuItem("Medium");
        mediumDifficultyMenuItem.setActionCommand(CMD_SELECT_MED_DIFF);
        mediumDifficultyMenuItem.addActionListener(this);
        difficultyLevelMenu.add(mediumDifficultyMenuItem);
        highDifficultyMenuItem = new JMenuItem("High");
        highDifficultyMenuItem.setActionCommand(CMD_SELECT_HIGH_DIFF);
        highDifficultyMenuItem.addActionListener(this);
        difficultyLevelMenu.add(highDifficultyMenuItem);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (this.callBackHandler != null) {
            callBackHandler.onEvent(event.getActionCommand());
        }
    }
}
