package com.example.minesweeper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

public class GamePanel extends JPanel implements MouseListener {
    private static final Dimension CELL_SIZE = new Dimension(35, 35);
    private MineSelectionListener mineSelectionListener;
    private final DifficultyLevel difficultyLevel;
    private final Cell[][] cells;
    private boolean createMines;
    private boolean isMinesCreated;
    private CellPosition startCellPosition = new CellPosition();
    private boolean isGameOver;

    void setMineSelectionListener(MineSelectionListener mineSelectionListener) {
        this.mineSelectionListener = mineSelectionListener;
    }

    GamePanel(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
        cells = new Cell[difficultyLevel.getRowCount()][difficultyLevel.getColumnCount()];
        createGameBoard();
        setVisible(true);
    }

    private void createGameBoard() {
        int unOpenedNonMineCellCount = 0;
        removeAll();
        setLayout(new GridLayout(difficultyLevel.getRowCount(), difficultyLevel.getColumnCount(), 0, 0));
        invalidate();
        if (createMines) {
            for (int rowIndex = 0; rowIndex < difficultyLevel.getRowCount(); rowIndex++) {
                for (int columnIndex = 0; columnIndex < difficultyLevel.getColumnCount(); columnIndex++) {
                    if (cells[rowIndex][columnIndex] == null) {
                        cells[rowIndex][columnIndex] = new Cell();
                    }
                }
            }
            final Set<CellPosition> minedCells = getMinedCells();
            minedCells.forEach(cp -> cells[cp.getRow()][cp.getColumn()].setHasMine());
        }
        for (int rowIndex = 0; rowIndex < difficultyLevel.getRowCount(); rowIndex++) {
            for (int columnIndex = 0; columnIndex < difficultyLevel.getColumnCount(); columnIndex++) {
                if (cells[rowIndex][columnIndex] == null) {
                    cells[rowIndex][columnIndex] = new Cell();
                }
                cells[rowIndex][columnIndex].setCellPosition(rowIndex, columnIndex);
                if (cells[rowIndex][columnIndex].isOpened() || (isGameOver && cells[rowIndex][columnIndex].isHasMine())) {
                    JLabel cellLabel = new JLabel();
                    cellLabel.setHorizontalAlignment(JLabel.CENTER);
                    cellLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
                    if (cells[rowIndex][columnIndex].isHasMine()) {
                        cellLabel.setText("X");
                    } else {
                        int mineCount = countMinesNearBy(cells[rowIndex][columnIndex]);
                        if (mineCount > 0) {
                            cellLabel.setText(String.valueOf(mineCount));
                        }
                    }
                    cellLabel.setPreferredSize(CELL_SIZE);
                    add(cellLabel);
                } else {
                    JButton cellButton = new JButton();
                    if (!cells[rowIndex][columnIndex].isHasMine() && !cells[rowIndex][columnIndex].isOpened()) {
                        unOpenedNonMineCellCount++;
                    }
                    cellButton.setHorizontalAlignment(JButton.CENTER);
                    if (!isGameOver) {
                        cellButton.addMouseListener(this);
                    }
                    cellButton.setPreferredSize(CELL_SIZE);
                    final ButtonAction buttonAction = new ButtonAction();
                    buttonAction.putValue("Y", rowIndex);
                    buttonAction.putValue("X", columnIndex);
                    cellButton.setAction(buttonAction);
                    if (cells[rowIndex][columnIndex].isMarkedAsMine()) {
                        cellButton.setText("!");
                    }
                    add(cellButton);
                }
            }
        }
        if (!isGameOver && isMinesCreated && unOpenedNonMineCellCount == 0) {
            if (mineSelectionListener != null) {
                mineSelectionListener.onGameEnd();
            }
            JOptionPane.showMessageDialog(this, "Congragulations!!!");
        }
        revalidate();
    }

    private int countMinesNearBy(Cell cell) {
        int mineCount = 0;
        // n:m-1
        if ((cell.getColumn() - 1) >= 0 && cells[cell.getRow()][cell.getColumn() - 1].isHasMine()) {
            mineCount++;
        }
        //n:m+1
        if ((cell.getColumn() + 1) < difficultyLevel.getColumnCount() && cells[cell.getRow()][cell.getColumn() + 1].isHasMine()) {
            mineCount++;
        }
        //n-1:m
        if ((cell.getRow() - 1) >= 0 && cells[cell.getRow() - 1][cell.getColumn()].isHasMine()) {
            mineCount++;
        }
        //n+1:m
        if ((cell.getRow() + 1) < difficultyLevel.getRowCount() && cells[cell.getRow() + 1][cell.getColumn()].isHasMine()) {
            mineCount++;
        }

        //n-1:m-1
        if ((cell.getColumn() - 1) >= 0 && (cell.getRow() - 1) >= 0 && cells[cell.getRow() - 1][cell.getColumn() - 1].isHasMine()) {
            mineCount++;
        }
        //n-1:m+1
        if ((cell.getColumn() + 1) < difficultyLevel.getColumnCount() && (cell.getRow() - 1) >= 0 && cells[cell.getRow() - 1][cell.getColumn() + 1].isHasMine()) {
            mineCount++;
        }
        //n-1:m-1
        if ((cell.getRow() + 1) < difficultyLevel.getRowCount() && (cell.getColumn() - 1) >= 0 && cells[cell.getRow() + 1][cell.getColumn() - 1].isHasMine()) {
            mineCount++;
        }
        //n+1:m+1
        if ((cell.getRow() + 1) < difficultyLevel.getRowCount() && (cell.getColumn() + 1) < difficultyLevel.getColumnCount() && cells[cell.getRow() + 1][cell.getColumn() + 1].isHasMine()) {
            mineCount++;
        }
        return mineCount;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        if (mineSelectionListener != null) {
            mineSelectionListener.onStartGame();
        }
        final JButton button = (JButton) event.getSource();
        ButtonAction buttonAction = (ButtonAction) button.getAction();
        int clickedColumn = (int) buttonAction.getValue("X");
        int clickedRow = (int) buttonAction.getValue("Y");
        startCellPosition.setRow(clickedRow);
        startCellPosition.setColumn(clickedColumn);
        if (SwingUtilities.isRightMouseButton(event)) {
            if (cells[clickedRow][clickedColumn].isMarkedAsMine()) {
                cells[clickedRow][clickedColumn].clearMineMark();
                if (mineSelectionListener != null) {
                    mineSelectionListener.onMineDeselect();
                }
            } else {
                cells[clickedRow][clickedColumn].markAsMine();
                if (mineSelectionListener != null) {
                    mineSelectionListener.onMineSelect();
                }
            }
            createGameBoard();
        } else {
            if (!cells[clickedRow][clickedColumn].isMarkedAsMine()) {
                cells[clickedRow][clickedColumn].open();
            }
            if (cells[clickedRow][clickedColumn].isHasMine()) {
                isGameOver = true;
            }
            if (!isMinesCreated) {
                createMines = true;
            }
            createGameBoard();
            createMines = false;
            if (isGameOver) {
                JOptionPane.showMessageDialog(this, "Game Over!!!");
                if (mineSelectionListener != null) {
                    mineSelectionListener.onGameEnd();
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent event) {
        // This method is not gonna be used.
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        // This method is not gonna be used.
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        // This method is not gonna be used.
    }

    @Override
    public void mouseExited(MouseEvent event) {
        // This method is not gonna be used.
    }

    private Set<CellPosition> getMinedCells() {
        Random random = new Random();
        Set<CellPosition> cellPositions = new HashSet<>();
        while (cellPositions.size() < difficultyLevel.getNumberOfMines()) {
            CellPosition cellPosition = new CellPosition();
            cellPosition.setRow(random.nextInt(difficultyLevel.getRowCount()));
            cellPosition.setColumn(random.nextInt(difficultyLevel.getColumnCount()));
            if (!startCellPosition.equals(cellPosition)) {
                cellPositions.add(cellPosition);
            }
        }
        isMinesCreated = true;
        return Collections.unmodifiableSet(cellPositions);
    }

    private static final class CellPosition {
        private int row;
        private int column;

        int getRow() {
            return row;
        }

        void setRow(int row) {
            this.row = row;
        }

        int getColumn() {
            return column;
        }

        void setColumn(int column) {
            this.column = column;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CellPosition that = (CellPosition) o;
            return row == that.row &&
                    column == that.column;
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, column);
        }
    }
}
