package com.example.minesweeper;

class Cell {
    private boolean hasMine;
    private boolean isMarkedAsMine;
    private boolean isOpened;
    private int row;
    private int column;

    boolean isHasMine() {
        return hasMine;
    }

    void setHasMine() {
        this.hasMine = true;
    }

    boolean isMarkedAsMine() {
        return isMarkedAsMine;
    }

    void markAsMine() {
        isMarkedAsMine = true;
    }

    void clearMineMark() {
        isMarkedAsMine = false;
    }

    boolean isOpened() {
        return isOpened;
    }

    void open() {
        isOpened = true;
    }

    int getRow() {
        return row;
    }

    void setRow(int row) {
        this.row = row;
    }

    int getColumn() {
        return column;
    }

    void setColumn(int column) {
        this.column = column;
    }

    void setCellPosition(int row, int column) {
        setRow(row);
        setColumn(column);
    }
}
