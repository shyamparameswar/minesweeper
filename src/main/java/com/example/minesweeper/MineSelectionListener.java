package com.example.minesweeper;

public interface MineSelectionListener {
    void onMineSelect();

    void onMineDeselect();

    void onStartGame();

    void onGameEnd();
}
