package com.example.minesweeper;

import javax.swing.*;

public class GameWindow implements MenuCallBackHandler, MineSelectionListener {
    private final JFrame gameFrame = new JFrame("Mine Sweeper");
    private GamePanel gamePanel;
    private DifficultyLevel difficultyLevel = DifficultyLevel.LOW;
    private StatusPanel statusPanel = new StatusPanel();

    private GameWindow() {
        gameFrame.setJMenuBar(new MineSweeperMenuBar(this));
        gameFrame.setLayout(new BoxLayout(gameFrame.getContentPane(), BoxLayout.PAGE_AXIS));
        gameFrame.add(statusPanel);
        gamePanel = new GamePanel(difficultyLevel);
        gamePanel.setMineSelectionListener(this);
        gameFrame.add(gamePanel);
        gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gameFrame.pack();
        gameFrame.setVisible(true);
        gameFrame.setResizable(false);
    }

    public static void main(String[] args) {
        new GameWindow();
    }

    @Override
    public void onEvent(String eventName) {
        boolean resetGame = false;
        switch (eventName) {
            case MineSweeperMenuBar.CMD_SELECT_LOW_DIFF:
                difficultyLevel = DifficultyLevel.LOW;
                resetGame = true;
                break;
            case MineSweeperMenuBar.CMD_SELECT_MED_DIFF:
                difficultyLevel = DifficultyLevel.MEDIUM;
                resetGame = true;
                break;
            case MineSweeperMenuBar.CMD_SELECT_HIGH_DIFF:
                difficultyLevel = DifficultyLevel.HIGH;
                resetGame = true;
                break;
            case MineSweeperMenuBar.CMD_RESET_GAME_EVENT:
                resetGame = true;
                break;
            default:
                resetGame = false;
                break;
        }
        if (resetGame) {
            statusPanel.reset();
            gameFrame.remove(gamePanel);
            gamePanel = new GamePanel(difficultyLevel);
            gamePanel.setMineSelectionListener(this);
            gameFrame.add(gamePanel);
            gameFrame.pack();
        }
    }

    @Override
    public void onMineSelect() {
        statusPanel.incrementMineSelection();
    }

    @Override
    public void onMineDeselect() {
        statusPanel.decrementMineSelection();
    }

    @Override
    public void onStartGame() {
        statusPanel.startTimer();
    }

    @Override
    public void onGameEnd() {
        statusPanel.stopTimer();
    }
}