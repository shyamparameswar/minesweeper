package com.example.minesweeper;

public interface MenuCallBackHandler {
    void onEvent(String eventName);
}
